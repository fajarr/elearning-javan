<?php

use Modules\Kabupaten\Controllers\KabupatenController;

$router->group(
    [
        'prefix' => config('modules.kabupaten.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.kabupaten.route.middleware'),
    ],
    function ($router) {
        $router->resource('kabupaten', KabupatenController::class);
    }
);
