<?php

namespace Modules\Kabupaten\Controllers;

use Illuminate\Routing\Controller;
use Modules\Kabupaten\Requests\Store;
use Modules\Kabupaten\Requests\Update;
use Modules\Kabupaten\Models\Kabupaten;
use Modules\Kabupaten\Tables\KabupatenTableView;
use Modules\Provinsi\Models\Provinsi;

class KabupatenController extends Controller
{
    public function index()
    {
        return KabupatenTableView::make()->view('kabupaten::index');
    }

    public function create()
    {
        $provinsi = Provinsi::all()->pluck('name', 'id');
        return view('kabupaten::create', compact('provinsi'));
    }

    public function store(Store $request)
    {
        Kabupaten::create($request->validated());

        return redirect()->back()->withSuccess('Kabupaten saved');
    }

    public function show(Kabupaten $kabupaten)
    {
        return view('kabupaten::show', compact('kabupaten'));
    }

    public function edit(Kabupaten $kabupaten)
    {
        return view('kabupaten::edit', compact('kabupaten'));
    }

    public function update(Update $request, Kabupaten $kabupaten)
    {
        $kabupaten->update($request->validated());

        return redirect()->back()->withSuccess('Kabupaten saved');
    }

    public function destroy(Kabupaten $kabupaten)
    {
        $kabupaten->delete();

        return redirect()->back()->withSuccess('Kabupaten deleted');
    }
}
