@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Kabupaten">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::kabupaten.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
