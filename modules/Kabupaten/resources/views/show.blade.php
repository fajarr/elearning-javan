@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kabupaten.index') }}"></x-backlink>

    <x-panel title="Detil Kabupaten">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $kabupaten->id }}</td></tr>
        <tr><td>Name</td><td>{{ $kabupaten->name }}</td></tr>
        <tr><td>Provinsi Id</td><td>{{ $kabupaten->provinsi_id }}</td></tr>
        <tr><td>Created At</td><td>{{ $kabupaten->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $kabupaten->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
