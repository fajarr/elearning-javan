@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kabupaten.index') }}"></x-backlink>

    <x-panel title="Tambah Kabupaten">
        {!! form()->post(route('modules::kabupaten.store'))->horizontal()->multipart()->attribute('autocomplete', 'off') !!}
        @include('kabupaten::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
