@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kabupaten.index') }}"></x-backlink>

    <x-panel title="Edit Kabupaten">
        {!! form()->bind($kabupaten)->put(route('modules::kabupaten.update', $kabupaten->getKey()))->horizontal()->multipart() !!}
        @include('kabupaten::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
