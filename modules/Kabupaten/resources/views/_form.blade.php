{!! form()->text('name')->label('Name') !!}
{!! form()->select('provinsi_id', $provinsi)->label('Provinsi')->placeholder('Pilih Provinsi') !!}
{!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('modules::kabupaten.index'))
]) !!}
