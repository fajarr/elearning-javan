<?php

namespace Modules\Kabupaten\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Kabupaten\Models\Kabupaten;

class KabupatenTableView extends TableView
{
    public function source()
    {
        return Kabupaten::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name', 'Nama Kabupaten')->sortable(),
            Text::make('provinsi.name', 'Nama Provinsi')->sortable(),
            RestfulButton::make('modules::kabupaten', 'Action'),
        ];
    }
}
