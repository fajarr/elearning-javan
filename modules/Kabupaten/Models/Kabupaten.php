<?php

namespace Modules\Kabupaten\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Kecamatan\Models\Kecamatan;
use Modules\Provinsi\Models\Provinsi;

class Kabupaten extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'kabupaten';

    protected $guarded = [];

    protected $searchableColumns = ["name",];

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class);
    }

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class);
    }
}
