<?php

namespace Modules\Provinsi\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Kabupaten\Models\Kabupaten;

class Provinsi extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'provinsi';

    protected $guarded = [];

    protected $searchableColumns = ["name",];

    public function kabupaten()
    {
        return $this->hasMany(Kabupaten::class);
    }
}
