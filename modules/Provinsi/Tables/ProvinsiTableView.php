<?php

namespace Modules\Provinsi\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Provinsi\Models\Provinsi;

class ProvinsiTableView extends TableView
{
    public function source()
    {
        return Provinsi::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name')->sortable(),
            RestfulButton::make('modules::provinsi'),
        ];
    }
}
