<?php

use Modules\Provinsi\Controllers\ProvinsiController;

$router->group(
    [
        'prefix' => config('modules.provinsi.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.provinsi.route.middleware'),
    ],
    function ($router) {
        $router->resource('provinsi', ProvinsiController::class);
    }
);
