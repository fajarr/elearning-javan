<?php

namespace Modules\Provinsi\Controllers;

use Illuminate\Routing\Controller;
use Modules\Provinsi\Requests\Store;
use Modules\Provinsi\Requests\Update;
use Modules\Provinsi\Models\Provinsi;
use Modules\Provinsi\Tables\ProvinsiTableView;

class ProvinsiController extends Controller
{
    public function index()
    {
        return ProvinsiTableView::make()->view('provinsi::index');
    }

    public function create()
    {
        return view('provinsi::create');
    }

    public function store(Store $request)
    {
        Provinsi::create($request->validated());

        return redirect()->back()->withSuccess('Provinsi saved');
    }

    public function show(Provinsi $provinsi)
    {
        return view('provinsi::show', compact('provinsi'));
    }

    public function edit(Provinsi $provinsi)
    {
        return view('provinsi::edit', compact('provinsi'));
    }

    public function update(Update $request, Provinsi $provinsi)
    {
        $provinsi->update($request->validated());

        return redirect()->back()->withSuccess('Provinsi saved');
    }

    public function destroy(Provinsi $provinsi)
    {
        $provinsi->delete();

        return redirect()->back()->withSuccess('Provinsi deleted');
    }
}
