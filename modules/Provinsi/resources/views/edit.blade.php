@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::provinsi.index') }}"></x-backlink>

    <x-panel title="Edit Provinsi">
        {!! form()->bind($provinsi)->put(route('modules::provinsi.update', $provinsi->getKey()))->horizontal()->multipart() !!}
        @include('provinsi::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
