@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::provinsi.index') }}"></x-backlink>

    <x-panel title="Detil Provinsi">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $provinsi->id }}</td></tr>
        <tr><td>Name</td><td>{{ $provinsi->name }}</td></tr>
        <tr><td>Created At</td><td>{{ $provinsi->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $provinsi->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
