@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::provinsi.index') }}"></x-backlink>

    <x-panel title="Tambah Provinsi">
        {!! form()->post(route('modules::provinsi.store'))->horizontal()->multipart()->attribute('autocomplete', 'off') !!}
        @include('provinsi::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
