@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Provinsi">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::provinsi.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
