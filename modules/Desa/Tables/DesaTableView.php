<?php

namespace Modules\Desa\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Desa\Models\Desa;

class DesaTableView extends TableView
{
    public function source()
    {
        return Desa::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name', 'Nama Desa/Kelurahan')->sortable(),
            Text::make('kecamatan.name', 'Nama Kecamatan')->sortable(),
            RestfulButton::make('modules::desa'),
        ];
    }
}
