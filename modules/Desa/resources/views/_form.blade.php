{!! form()->text('name')->label('Name') !!}
{!! form()->select('kecamatan_id', $kecamatan)->label('Kecamatan')->placeholder('Pilih Kecamatan') !!}
{!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('modules::desa.index'))
]) !!}
