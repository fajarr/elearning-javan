@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Desa">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::desa.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
