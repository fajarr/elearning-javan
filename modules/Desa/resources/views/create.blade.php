@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::desa.index') }}"></x-backlink>

    <x-panel title="Tambah Desa">
        {!! form()->post(route('modules::desa.store'))->horizontal()->multipart()->attribute('autocomplete', 'off') !!}
        @include('desa::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
