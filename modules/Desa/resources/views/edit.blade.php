@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::desa.index') }}"></x-backlink>

    <x-panel title="Edit Desa">
        {!! form()->bind($desa)->put(route('modules::desa.update', $desa->getKey()))->horizontal()->multipart() !!}
        @include('desa::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
