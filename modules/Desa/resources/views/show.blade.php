@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::desa.index') }}"></x-backlink>

    <x-panel title="Detil Desa">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $desa->id }}</td></tr>
        <tr><td>Name</td><td>{{ $desa->name }}</td></tr>
        <tr><td>Kecamatan Id</td><td>{{ $kecamatan }}</td></tr>
        <tr><td>Created At</td><td>{{ $desa->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $desa->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
