<?php

namespace Modules\Desa\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Kecamatan\Models\Kecamatan;

class Desa extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'desa';

    protected $guarded = [];

    protected $searchableColumns = ["name",];

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class);
    }
}
