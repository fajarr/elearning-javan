<?php

use Modules\Desa\Controllers\DesaController;

$router->group(
    [
        'prefix' => config('modules.desa.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.desa.route.middleware'),
    ],
    function ($router) {
        $router->resource('desa', DesaController::class);
    }
);
