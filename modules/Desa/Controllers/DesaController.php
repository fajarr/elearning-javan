<?php

namespace Modules\Desa\Controllers;

use Illuminate\Routing\Controller;
use Modules\Desa\Requests\Store;
use Modules\Desa\Requests\Update;
use Modules\Desa\Models\Desa;
use Modules\Desa\Tables\DesaTableView;
use Modules\Kecamatan\Models\Kecamatan;

class DesaController extends Controller
{
    public function index()
    {
        return DesaTableView::make()->view('desa::index');
    }

    public function create()
    {
        $kecamatan = Kecamatan::all()->pluck('name', 'id');
        return view('desa::create', compact('kecamatan'));
    }

    public function store(Store $request)
    {
        Desa::create($request->validated());

        return redirect()->back()->withSuccess('Desa saved');
    }

    public function show(Desa $desa)
    {
        $kecamatan = $desa->kecamatan->name;
        return view('desa::show', compact('desa', 'kecamatan'));
    }

    public function edit(Desa $desa)
    {
        $kecamatan = Kecamatan::all()->pluck('name', 'id');
        return view('desa::edit', compact('desa', 'kecamatan'));
    }

    public function update(Update $request, Desa $desa)
    {
        $desa->update($request->validated());

        return redirect()->back()->withSuccess('Desa saved');
    }

    public function destroy(Desa $desa)
    {
        $desa->delete();

        return redirect()->back()->withSuccess('Desa deleted');
    }
}
