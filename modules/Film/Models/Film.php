<?php

namespace Modules\Film\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Film extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'film';

    protected $guarded = [];

    protected $searchableColumns = ["title", "schedule",];

    public function user()
    {
        return $this->belongsToMany(User::class, 'film_user', 'film_id', 'user_id');
    }
}
