@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::film.index') }}"></x-backlink>

    <x-panel title="Edit Film">
        {!! form()->bind($film)->put(route('modules::film.update', $film->getKey()))->horizontal()->multipart() !!}
        @include('film::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
