@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::film.index') }}"></x-backlink>

    <x-panel title="Tambah Film">
        {!! form()->post(route('modules::film.store'))->horizontal()->multipart()->attribute('autocomplete', 'off') !!}
        @include('film::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
