@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Film">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::film.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
