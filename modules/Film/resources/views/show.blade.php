@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::film.index') }}"></x-backlink>

    <x-panel title="Detil Film">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $film->id }}</td></tr>
        <tr><td>Title</td><td>{{ $film->title }}</td></tr>
        <tr><td>Schedule</td><td>{{ $film->schedule }}</td></tr>
        <tr><td>Created At</td><td>{{ $film->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $film->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
