<?php

use Modules\Film\Controllers\FilmController;

$router->group(
    [
        'prefix' => config('modules.film.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.film.route.middleware'),
    ],
    function ($router) {
        $router->resource('film', FilmController::class);
    }
);
