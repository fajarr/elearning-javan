<?php

namespace Modules\Film\Controllers;

use Illuminate\Routing\Controller;
use Modules\Film\Requests\Store;
use Modules\Film\Requests\Update;
use Modules\Film\Models\Film;
use Modules\Film\Tables\FilmTableView;

class FilmController extends Controller
{
    public function index()
    {
        return FilmTableView::make()->view('film::index');
    }

    public function create()
    {
        return view('film::create');
    }

    public function store(Store $request)
    {
        Film::create($request->validated());

        return redirect()->back()->withSuccess('Film saved');
    }

    public function show(Film $film)
    {
        return view('film::show', compact('film'));
    }

    public function edit(Film $film)
    {
        return view('film::edit', compact('film'));
    }

    public function update(Update $request, Film $film)
    {
        $film->update($request->validated());

        return redirect()->back()->withSuccess('Film saved');
    }

    public function destroy(Film $film)
    {
        $film->delete();

        return redirect()->back()->withSuccess('Film deleted');
    }
}
