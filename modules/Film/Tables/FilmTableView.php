<?php

namespace Modules\Film\Tables;

use Laravolt\Suitable\Columns\Date;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Film\Models\Film;

class FilmTableView extends TableView
{
    public function source()
    {
        return Film::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('title')->sortable(),
            Raw::make(function ($film) {
                return date('l, d M Y, H:i', strtotime($film->schedule));
            }, 'Schedule')->sortable(),
            Raw::make(function ($film) {
                return count($film->user);
            }, 'Jumlah Penonton'),
            RestfulButton::make('modules::film'),
        ];
    }
}
