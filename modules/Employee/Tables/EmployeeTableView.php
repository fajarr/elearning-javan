<?php

namespace Modules\Employee\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Employee\Models\Employee;

class EmployeeTableView extends TableView
{
    protected function init()
    {
        $this->decorate(function (\Laravolt\Suitable\Builder $table) {
            $table->getDefaultSegment()->left('<a class="ui primary button" href="' . route('modules::employee.export-pdf') . '">Export to PDF</a>',
                '<a class="ui primary button" href="' . route('modules::employee.export-excel') . '">Export to Excel</a>');
        });
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name')->sortable(),
            Text::make('parent.name', 'Atasan')->sortable(),
            Text::make('company.name', 'Perusahaan')->sortable(),
            RestfulButton::make('modules::employee'),
        ];
    }
}
