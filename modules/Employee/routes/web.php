<?php

use Modules\Employee\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

Route::get('modules/employee/export-pdf', [\Modules\Employee\Controllers\EmployeesExportController::class, 'exportPDF'])->name('modules::employee.export-pdf');
Route::get('modules/employee/export-excel', [\Modules\Employee\Controllers\EmployeesExportController::class, 'exportExcel'])->name('modules::employee.export-excel');
Route::get('modules/employee/export', [\Modules\Employee\Controllers\EmployeesExportController::class, 'pass']);

$router->group(
    [
        'prefix' => config('modules.employee.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.employee.route.middleware'),
    ],
    function ($router) {
        $router->resource('employee', EmployeeController::class);
    }
);
