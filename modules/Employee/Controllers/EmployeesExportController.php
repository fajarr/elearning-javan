<?php

namespace Modules\Employee\Controllers;

use App\Exports\EmployeesExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Employee\Models\Employee;
use PDF;

class EmployeesExportController extends Controller
{
    /**
     * function to export to PDF
     */
    public function exportPDF()
    {
        $employees = Employee::all();
        $pdf = PDF::loadView('employee::employee-export', compact('employees'));
        return $pdf->stream('employees.pdf');
    }

    /**
     * function to export to Excel
     */
    public function exportExcel()
    {
        return Excel::download(new EmployeesExport, 'employees.xlsx');
    }

    /**
     * function to pass all employee to blade
     */
    public function pass()
    {
        $employees = Employee::all();
        return view('employee::employee-export', compact('employees'));
    }
}
