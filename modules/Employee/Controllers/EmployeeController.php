<?php

namespace Modules\Employee\Controllers;

use Illuminate\Routing\Controller;
use Modules\Company\Models\Company;
use Modules\Employee\Requests\Store;
use Modules\Employee\Requests\Update;
use Modules\Employee\Models\Employee;
use Modules\Employee\Tables\EmployeeTableView;

class EmployeeController extends Controller
{
    public function index()
    {
        $employee = Employee::with('parent')->get();
        return EmployeeTableView::make($employee)->view('employee::index');
    }

    public function create()
    {
        $atasan = Employee::all()->pluck('name', 'id');
        $company = Company::all()->pluck('name', 'id');
        return view('employee::create', compact('atasan', 'company'));
    }

    public function store(Store $request)
    {
        Employee::create($request->validated());

        return redirect()->back()->withSuccess('Employee saved');
    }

    public function show(Employee $employee)
    {
        return view('employee::show', compact('employee'));
    }

    public function edit(Employee $employee)
    {
        $atasan = Employee::all()->pluck('name', 'id');
        $company = Company::all()->pluck('name', 'id');
        return view('employee::edit', compact('employee', 'atasan', 'company'));
    }

    public function update(Update $request, Employee $employee)
    {
        $employee->update($request->validated());

        return redirect()->back()->withSuccess('Employee saved');
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->back()->withSuccess('Employee deleted');
    }
}
