<?php

namespace Modules\Employee\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Company\Models\Company;

class Employee extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'employee';

    protected $guarded = [];

    protected $searchableColumns = ["name",];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

//    public function downlines(){
//        return $this->hasMany(Employee::class, 'atasan_id', 'id');
//    }

    public function parent() {
        return $this->belongsTo(Employee::class, 'atasan_id', 'id');
    }
}
