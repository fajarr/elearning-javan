@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::employee.index') }}"></x-backlink>

    <x-panel title="Detil Employee">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $employee->id }}</td></tr>
        <tr><td>Name</td><td>{{ $employee->name }}</td></tr>
        <tr><td>Atasan Id</td><td>{{ $employee->atasan_id }}</td></tr>
        <tr><td>Company Id</td><td>{{ $employee->company_id }}</td></tr>
        <tr><td>Created At</td><td>{{ $employee->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $employee->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
