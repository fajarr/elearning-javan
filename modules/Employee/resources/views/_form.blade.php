{!! form()->text('name')->label('Name') !!}
{!! form()->select('atasan_id', $atasan)->label('Atasan')->placeholder('Pilih Atasan') !!}
{!! form()->select('company_id', $company)->label('Company')->placeholder('Choose Company') !!}
{!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('modules::employee.index'))
]) !!}
