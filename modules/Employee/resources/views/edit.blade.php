@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::employee.index') }}"></x-backlink>

    <x-panel title="Edit Employee">
        {!! form()->bind($employee)->put(route('modules::employee.update', $employee->getKey()))->horizontal()->multipart() !!}
        @include('employee::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
