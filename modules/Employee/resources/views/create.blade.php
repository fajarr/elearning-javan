@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::employee.index') }}"></x-backlink>

    <x-panel title="Tambah Employee">
        {!! form()->post(route('modules::employee.store'))->horizontal()->multipart() !!}
        @include('employee::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
