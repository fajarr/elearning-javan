<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>Employee</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 10pt;
        }

        table.content {
            width: 100%;
            border: 0.1mm solid black;
            border-collapse: collapse;
        }

        .content td {
            border: 0.1mm solid black;
            padding-left: 10px;
        }

        table thead th {
            background-color: #EEEEEE;
            text-align: center;
            border: 0.1mm solid #000000;
        }
    </style>
</head>
<body>

<table class="content" aria-describedby="employees">
    <thead>
    <tr>
        <th id="ID">ID</th>
        <th id="Name">Nama</th>
        <th id="Posisi">Posisi</th>
        <th id="Perusahaan">Perusahaan</th>
    </tr>
    </thead>
    <tbody>

    @foreach($employees as $e)
        <tr>
            <td>{{ $e->id }}</td>
            <td>{{ $e->name }}</td>

            @if($e->atasan_id === null)
                <td>CEO</td>
            @elseif($e->atasan_id === 1)
                <td>Direktur</td>
            @elseif ($e->atasan_id === 2 | $e->atasan_id === 3)
                <td>Manager</td>
            @else
                <td>Staff</td>
            @endif

            <td>{{ $e->company->name }}</td>
        </tr>
    @endforeach

    </tbody>
</table>
</body>
</html>
