@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::company.index') }}"></x-backlink>

    <x-panel title="Detil Company">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $company->id }}</td></tr>
        <tr><td>Name</td><td>{{ $company->name }}</td></tr>
        <tr><td>Address</td><td>{{ $company->address }}</td></tr>
        <tr><td>Created At</td><td>{{ $company->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $company->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
