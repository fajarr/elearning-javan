@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Company">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::company.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
