@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::company.index') }}"></x-backlink>

    <x-panel title="Edit Company">
        {!! form()->bind($company)->put(route('modules::company.update', $company->getKey()))->horizontal()->multipart() !!}
        @include('company::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
