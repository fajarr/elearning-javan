@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::company.index') }}"></x-backlink>

    <x-panel title="Tambah Company">
        {!! form()->post(route('modules::company.store'))->horizontal()->multipart() !!}
        @include('company::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
