<?php

use Modules\Company\Controllers\CompanyController;

$router->group(
    [
        'prefix' => config('modules.company.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.company.route.middleware'),
    ],
    function ($router) {
        $router->resource('company', CompanyController::class);
    }
);
