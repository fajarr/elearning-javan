<?php

namespace Modules\Company\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Employee\Models\Employee;

class Company extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'company';

    protected $guarded = [];

    protected $searchableColumns = ["name", "address",];

    public function employee()
    {
        return $this->hasMany(Employee::class, 'company_id');
    }
}
