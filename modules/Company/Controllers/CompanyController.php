<?php

namespace Modules\Company\Controllers;

use Illuminate\Routing\Controller;
use Modules\Company\Requests\Store;
use Modules\Company\Requests\Update;
use Modules\Company\Models\Company;
use Modules\Company\Tables\CompanyTableView;

class CompanyController extends Controller
{
    public function index()
    {
        return CompanyTableView::make()->view('company::index');
    }

    public function create()
    {
        return view('company::create');
    }

    public function store(Store $request)
    {
        Company::create($request->validated());

        return redirect()->back()->withSuccess('Company saved');
    }

    public function show(Company $company)
    {
        return view('company::show', compact('company'));
    }

    public function edit(Company $company)
    {
        return view('company::edit', compact('company'));
    }

    public function update(Update $request, Company $company)
    {
        $company->update($request->validated());

        return redirect()->back()->withSuccess('Company saved');
    }

    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()->back()->withSuccess('Company deleted');
    }
}
