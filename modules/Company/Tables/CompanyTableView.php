<?php

namespace Modules\Company\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Company\Models\Company;

class CompanyTableView extends TableView
{
    public function source()
    {
        return Company::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name')->sortable(),
            Text::make('address')->sortable(),
            RestfulButton::make('modules::company'),
        ];
    }
}
