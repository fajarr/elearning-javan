@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kecamatan.index') }}"></x-backlink>

    <x-panel title="Detil Kecamatan">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $kecamatan->id }}</td></tr>
        <tr><td>Name</td><td>{{ $kecamatan->name }}</td></tr>
        <tr><td>Kabupaten Id</td><td>{{ $kecamatan->kabupaten_id }}</td></tr>
        <tr><td>Created At</td><td>{{ $kecamatan->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $kecamatan->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
