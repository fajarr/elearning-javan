@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kecamatan.index') }}"></x-backlink>

    <x-panel title="Tambah Kecamatan">
        {!! form()->post(route('modules::kecamatan.store'))->horizontal()->multipart()->attribute('autocomplete', 'off') !!}
        @include('kecamatan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
