@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Kecamatan">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::kecamatan.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
