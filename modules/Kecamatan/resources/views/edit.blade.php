@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::kecamatan.index') }}"></x-backlink>

    <x-panel title="Edit Kecamatan">
        {!! form()->bind($kecamatan)->put(route('modules::kecamatan.update', $kecamatan->getKey()))->horizontal()->multipart() !!}
        @include('kecamatan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
