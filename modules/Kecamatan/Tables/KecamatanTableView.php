<?php

namespace Modules\Kecamatan\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Kecamatan\Models\Kecamatan;

class KecamatanTableView extends TableView
{
    public function source()
    {
        return Kecamatan::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('name', 'Nama Kecamatan')->sortable(),
            Text::make('kabupaten.name', 'Nama Kabupaten')->sortable(),
            RestfulButton::make('modules::kecamatan'),
        ];
    }
}
