<?php

use Modules\Kecamatan\Controllers\KecamatanController;

$router->group(
    [
        'prefix' => config('modules.kecamatan.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.kecamatan.route.middleware'),
    ],
    function ($router) {
        $router->resource('kecamatan', KecamatanController::class);
    }
);
