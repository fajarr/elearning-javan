<?php

namespace Modules\Kecamatan\Controllers;

use Illuminate\Routing\Controller;
use Modules\Kabupaten\Models\Kabupaten;
use Modules\Kecamatan\Requests\Store;
use Modules\Kecamatan\Requests\Update;
use Modules\Kecamatan\Models\Kecamatan;
use Modules\Kecamatan\Tables\KecamatanTableView;

class KecamatanController extends Controller
{
    public function index()
    {
        return KecamatanTableView::make()->view('kecamatan::index');
    }

    public function create()
    {
        $kabupaten = Kabupaten::all()->pluck('name', 'id');
        return view('kecamatan::create', compact('kabupaten'));
    }

    public function store(Store $request)
    {
        Kecamatan::create($request->validated());

        return redirect()->back()->withSuccess('Kecamatan saved');
    }

    public function show(Kecamatan $kecamatan)
    {
        return view('kecamatan::show', compact('kecamatan'));
    }

    public function edit(Kecamatan $kecamatan)
    {
        return view('kecamatan::edit', compact('kecamatan'));
    }

    public function update(Update $request, Kecamatan $kecamatan)
    {
        $kecamatan->update($request->validated());

        return redirect()->back()->withSuccess('Kecamatan saved');
    }

    public function destroy(Kecamatan $kecamatan)
    {
        $kecamatan->delete();

        return redirect()->back()->withSuccess('Kecamatan deleted');
    }
}
