<?php

namespace Modules\Kecamatan\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Desa\Models\Desa;
use Modules\Kabupaten\Models\Kabupaten;

class Kecamatan extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'kecamatan';

    protected $guarded = [];

    protected $searchableColumns = ["name",];

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class);
    }

    public function desa()
    {
        return $this->hasMany(Desa::class);
    }
}
