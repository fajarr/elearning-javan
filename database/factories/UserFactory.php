<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->firstName;
    return [
        'name' => $name,
        'email' => strtolower($name) . '@mail.com',
        'password' => Hash::make('password'), // password
        'email_verified_at' => now(),
        'status' => 'ACTIVE',
        'remember_token' => Str::random(10),
    ];
});
