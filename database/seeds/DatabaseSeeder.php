<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 2; $i++) {
            $user = factory(User::class)->create();
            DB::table('acl_role_user')->insert([
                'role_id' => '2',
                'user_id' => $user->id
            ]);
        }
    }
}
