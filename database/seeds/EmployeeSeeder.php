<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Employee\Models\Employee;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = Carbon::now();

        Employee::query()
            ->insert([
                ['name' => 'Pak Tono', 'atasan_id' => 1, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Pak Totok', 'atasan_id' => 1, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Bu Sinta', 'atasan_id' => 2, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Bu Novi', 'atasan_id' => 3, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Andre', 'atasan_id' => 4, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Dono', 'atasan_id' => 4, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Ismir', 'atasan_id' => 5, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
                ['name' => 'Anto', 'atasan_id' => 5, 'company_id' => 1, 'created_at' => $now, 'updated_at' => $now],
            ]);
    }
}
