<?php
function countVowels($txt)
{
    $low = strtolower($txt);
    $arr = str_split($low);
    $vowels = [];
    $a_val = false;
    $i_val = false;
    $u_val = false;
    $e_val = false;
    $o_val = false;
    $sum = 0;

    for ($i = 0; $i < sizeOf($arr); $i++) {
        if ($arr[$i] == 'a') {
            if ($a_val == false) {
                array_push($vowels, 'a');
                $a_val = true;
            } else {
                $sum++;
            }
        }
        if ($arr[$i] == 'i') {
            if ($i_val == false) {
                array_push($vowels, 'i');
                $i_val = true;
            } else {
                $sum++;
            }
        }
        if ($arr[$i] == 'u') {
            if ($u_val == false) {
                array_push($vowels, 'u');
                $u_val = true;
            } else {
                $sum++;
            }
        }
        if ($arr[$i] == 'e') {
            if ($e_val == false) {
                array_push($vowels, 'e');
                $e_val = true;
            } else {
                $sum++;
            }
        }
        if ($arr[$i] == 'o') {
            if ($o_val == false) {
                array_push($vowels, 'o');
                $o_val = true;
            } else {
                $sum++;
            }
        }
    }

    echo($low . " = ");
    if (sizeof($vowels) == 0) {
        echo("tidak ada");
    } elseif (sizeof($vowels) == 1) {
        echo($sum . " yaitu " . $vowels[0]);
    } elseif (sizeof($vowels) > 1) {
        echo($sum . " yaitu");
        for ($i = 0; $i < sizeof($vowels); $i++) {
            if ($i != sizeof($vowels) - 1) {
                echo(" " . $vowels[$i]);
            } else {
                echo(" dan " . $vowels[$i]);
            }
        }
    }
}

countVowels('manupraba');
