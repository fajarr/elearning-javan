# cari CEO
SELECT *
FROM employee
WHERE atasan_id IS NULL;

# cari direktur
SELECT directive.name as "Nama Direktur"
FROM employee directive
         JOIN employee ceo
              ON directive.atasan_id = ceo.id
WHERE ceo.atasan_id IS NULL;

# cari manager
SELECT manager.name as "Nama Manager"
FROM employee manager
         JOIN employee directive
              ON manager.atasan_id = directive.id
         JOIN employee ceo
              ON directive.atasan_id = ceo.id
WHERE directive.atasan_id = 1;

# cari staff
SELECT staff.name as "Nama Staff"
FROM employee staff
         JOIN employee manager
              ON manager.id = staff.atasan_id
         JOIN employee directive
              ON directive.id = manager.atasan_id
         JOIN employee ceo
              ON ceo.id = directive.atasan_id
WHERE manager.atasan_id = 2 OR 3;

# cari bawahan
WITH RECURSIVE cte (id, name, atasan_id, company_id)
    AS (
        SELECT id,
               name,
               atasan_id,
               company_id
        FROM employee
        WHERE name = 'Bu Sinta'


        UNION ALL

        SELECT bawahan.id,
               bawahan.name,
               bawahan.atasan_id,
               bawahan.company_id
        FROM employee bawahan
                 INNER JOIN cte
                            ON bawahan.atasan_id = cte.id
    )

SELECT COUNT(id) AS "Jumlah Bawahan"
FROM cte;