<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Home')->name('home');
Route::get('/dashboard', 'Dashboard')->name('dashboard')->middleware('auth');

Route::get('/complaints', 'ComplaintsController@index')->name('complaints.index');
Route::get('/complaints/result', 'ComplaintsController@data')->name('complaints.result');
