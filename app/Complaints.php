<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Complaints extends Model
{
    protected $table = 'complaints';

    public function getResponse($company)
    {
        $query =
            "SELECT Company,
                       COUNT(CASE WHEN `Company_Response_to_Consumer` = 'Closed' THEN id END) AS 'Closed',
                       COUNT(CASE WHEN `Company_Response_to_Consumer` = 'Closed with explanation' THEN id END ) AS 'Closed_with_explanation',
                       COUNT(CASE WHEN `Company_Response_to_Consumer` = 'Closed with non-monetary relief' THEN id END) AS 'Closed_with_non_monetary_relief',
                       COUNT(CASE WHEN `Company_Response_to_Consumer` = 'Closed with monetary relief' THEN id END) AS 'Closed_with_monetary_relief',
                       COUNT(CASE WHEN `Company_Response_to_Consumer` = 'Untimely response' THEN id END) AS 'Untimely_response'
            FROM `complaints`
            WHERE `Company` = ?
            GROUP BY Company";

        return DB::select($query, [$company]);
    }

    public function getCompany()
    {
        return DB::table($this->table)
            ->select('Company')
            ->distinct()
            ->orderBy('Company')
            ->get();
    }
}
