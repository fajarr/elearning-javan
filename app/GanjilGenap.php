<?php

namespace App;

class GanjilGenap
{
    //
    public function check(int $a, int $b)
    {
        $result = [];
        for ($i = $a; $i <= $b; $i++) {
            if ($i % 2 == 0) {
                array_push($result, "Angka " . $i . " adalah genap");
            } else {
                array_push($result, "Angka " . $i . " adalah ganjil");
            }
        }
        return $result;
    }
}
