<?php

declare(strict_types=1);

namespace App\Tables;

use Illuminate\Support\Facades\Auth;
use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Film\Models\Film;

class PenontonTableView extends TableView
{
    protected $title = 'List Film';

    protected function source()
    {
        return Film::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('number', 'No'),
            Text::make('title', 'Title'),
            Text::make('title')->sortable(),
            Raw::make(function ($film) {
                return date('l, d M Y, H:i', strtotime($film->schedule));
            }, 'Schedule')->sortable(),
            Raw::make(function ($film) {
                return "<a class='ui positive button' href='" . route('film.show', $film->id) . "'>Detail</a>";
            }, 'Action'),
        ];
    }
}
