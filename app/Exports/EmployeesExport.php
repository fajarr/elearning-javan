<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Modules\Employee\Models\Employee;

class EmployeesExport implements FromView
{

    public function view(): View
    {
        return view('employee::employee-export', [
            'employees' => Employee::all()
        ]);
    }
}
