<?php

namespace App;


class Vowels
{
    //
    public function count($txt)
    {
        $arr = str_split(strtolower($txt));
        $vowels = [];
        $a_val = false;
        $i_val = false;
        $u_val = false;
        $e_val = false;
        $o_val = false;

        for ($i = 0; $i < sizeOf($arr); $i++) {
            if ($arr[$i] == 'a') {
                if ($a_val == false) {
                    array_push($vowels, 'a');
                    $a_val = true;
                }
            }
            if ($arr[$i] == 'i') {
                if ($i_val == false) {
                    array_push($vowels, 'i');
                    $i_val = true;
                }
            }
            if ($arr[$i] == 'u') {
                if ($u_val == false) {
                    array_push($vowels, 'u');
                    $u_val = true;
                }
            }
            if ($arr[$i] == 'e') {
                if ($e_val == false) {
                    array_push($vowels, 'e');
                    $e_val = true;
                }
            }
            if ($arr[$i] == 'o') {
                if ($o_val == false) {
                    array_push($vowels, 'o');
                    $o_val = true;
                }
            }
        }
        return $vowels;
    }
}
