<?php

namespace App\Http\Controllers;

use App\Complaints;
use Illuminate\Http\Request;

class ComplaintsController extends Controller
{
    private $complaints;

    /**
     * ComplaintsController constructor.
     */
    public function __construct(Complaints $complaints)
    {
        $this->complaints = $complaints;
    }

    public function index()
    {
        $company = $this->complaints->getCompany()->pluck('Company', 'Company');

        return view('complaints.index', compact('company'));
    }

    public function data(Request $request)
    {
        $result = $this->complaints->getResponse($request->company);

        return view('complaints.result', compact('result'));
    }
}
