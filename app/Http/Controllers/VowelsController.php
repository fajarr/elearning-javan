<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vowels;

class VowelsController extends Controller
{
    //
    public function index()
    {
        return view('count_vowels');
    }

    /**
     *
     * @param Vowels $vowels
     */
    public function check(Vowels $vowels, Request $request)
    {
        $txt = strip_tags($request->input('txt'));
        $result = $vowels->count($txt);
        return view('vowels_result', compact('txt', 'result'));
    }
}
