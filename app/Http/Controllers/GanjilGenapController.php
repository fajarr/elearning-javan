<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GanjilGenap;

class GanjilGenapController extends Controller
{
    //
    public function index()
    {
        return view('ganjil_genap');
    }

    /**
     * cek apakah a  sampai b  adalah ganjil atau genap.
     *
     * @param GanjilGenap $ganjilGenap
     */
    public function check(GanjilGenap $ganjilGenap, Request $request)
    {
        $a = $request->input('a');
        $b = $request->input('b');
        $result = $ganjilGenap->check($a, $b);
        return view('hasil-ganjil_genap', compact('a', 'b', 'result'));
    }
}
