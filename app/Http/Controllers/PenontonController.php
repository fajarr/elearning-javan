<?php

namespace App\Http\Controllers;

use App\Tables\PenontonTableView;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Modules\Film\Models\Film;

class PenontonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return PenontonTableView|Application|Factory|View
     */
    public function index()
    {
        //
        $film = Auth::user()->film;
        return PenontonTableView::make()->view('user.film.index', compact('film'));
    }

    public function attach(Film $film)
    {
        $user = \auth()->user();
        $user->film()->attach($film->id);

        return redirect()->route('film.index')->withSuccess('Successfully added this movie!');
    }

    public function detach(Film $film)
    {
        $user = \auth()->user();
        $user->film()->detach($film->id);

        return redirect()->route('film.index')->withSuccess('Successfully removed this movie!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|Response|View
     */
    public function show(Film $film)
    {
        //
        return view('user.film.show', compact('film'));
    }
}
