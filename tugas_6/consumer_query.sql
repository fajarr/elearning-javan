LOAD DATA INFILE 'D:\ConsumerComplaints.csv'
    INTO TABLE complaints
    FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES
    (Date_Received, Product_Name, Sub_Product, Issue, Sub_Issue, Consumer_Complaint_Narrative, Company_Public_Response,
     Company, State_Name, Zip_Code, Tags, Consumer_Consent_Provider, Submitted_via, Date_Sent_to_Company,
     Company_Response_to_Consumer, Timely_Response, Consumer_Disputed, Complaint_ID);

# Hitung Jumlah Komplain Per Bulan
SELECT COUNT(id) AS "Complaints Total per Month", MONTH(Date_Received) AS "Month"
FROM complaints
WHERE MONTH(Date_Received)
GROUP BY MONTH(Date_Received)
ORDER BY MONTH(Date_Received);

# Komplain yang Memiliki Tags 'Older American'
SELECT COUNT(id)
FROM complaints
WHERE Tags = 'Older American';

# Data Nama Perusahaan, Jumlah Company Response to Consumer
SELECT Company,
       COUNT(CASE WHEN Company_Response_to_Consumer = 'Closed' THEN id END) AS 'Closed',
       COUNT(CASE WHEN Company_Response_to_Consumer = 'Closed with explanation' THEN id END ) AS 'Closed with explanation',
       COUNT(CASE WHEN Company_Response_to_Consumer = 'Closed with non-monetary relief' THEN id END) AS 'Closed with non-monetary relief',
       COUNT(CASE WHEN Company_Response_to_Consumer = 'Closed with monetary relief' THEN id END) AS 'Closed with monetary relief',
       COUNT(CASE WHEN Company_Response_to_Consumer = 'Untimely response' THEN id END) AS 'Untimely response'
FROM complaints
WHERE Company = 'Wells Fargo & Company';