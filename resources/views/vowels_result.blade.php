@extends('template')
@section('content')
    <div class="ui divider hidden"></div>
    <div class="ui grid equal">
        <div class="row">
            <div class="ten wide column centered">
                <form method="post" action="{{ route('count-vowels.check') }}" class="ui form">
                    @csrf
                    <div class="field">
                        <label for="txt">Nama</label>
                        <input type="text" id="txt" name="txt" placeholder="Input" required>
                    </div>
                    <button class="ui button" type="submit">Submit</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="ten wide column centered">
                <h4>Hasil</h4>
                {{ $txt . " = " }}

                @if(sizeof($result) == 0)
                    tidak ada
                @elseif(sizeof($result) == 1)
                    {{ count($result) . " yaitu " . $result[0] }}
                @elseif(sizeof($result) > 1)
                    {{ count($result) .  " yaitu " }}
                    @for($i = 0; $i < sizeof($result); $i++)
                        @if($i != sizeof($result) -1)
                            {{" " . $result[$i] }}
                        @else
                            {{" dan " . $result[$i] }}
                        @endif
                    @endfor
                @endif
            </div>
        </div>
    </div>
@stop
