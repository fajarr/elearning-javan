@extends('vendor.laravolt.layouts.app')
@section('content')

    <div class="ui grid">
        <div class="four wide column">
                <h4>Your Films</h4>
                <div class="ui divider hidden"></div>

                @foreach($film as $item)
                    <div class="ui cards">
                        <div class="card">
                            <div class="content">
                                <div class="header">{{ $item['title'] }}</div>
                            </div>
                            <div class="content">
                                <h4 class="ui sub header">Schedule</h4>
                                <div class="ui small feed">
                                    <div class="content">
                                        {{ date('l, d M Y, H:i', strtotime($item->schedule)) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>

        <div class="twelve wide column">
            {!! $table !!}
        </div>
    </div>
@stop
