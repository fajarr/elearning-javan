@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('film.index') }}"></x-backlink>

    <x-panel title="Detil Film">
        <table class="ui table definition">
            <tr>
                <td>Id</td>
                <td>{{ $film->id }}</td>
            </tr>
            <tr>
                <td>Title</td>
                <td>{{ $film->title }}</td>
            </tr>
            <tr>
                <td>Schedule</td>
                <td>{{ $film->schedule }}</td>
            </tr>
        </table>

        <table>
            <tr>
                <td>
                    <form action="{{ route('film.attach', $film->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <button class="ui labeled icon button green">
                            <i class="thumbs up icon"></i>
                            Tonton
                        </button>
                    </form>
                </td>

                <td>
                    <form action="{{ route('film.detach', $film->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <button class="ui labeled icon button red">
                            <i class="thumbs down icon"></i>
                            Batal
                        </button>
                    </form>
                </td>
            </tr>
        </table>
    </x-panel>

@stop
