@extends('template')
@section('content')
    <div class="ui divider hidden"></div>
    <div class="ui grid equal">
        <div class="row">
            <div class="ten wide column centered">
                <form method="post" action="{{ route('count-vowels.check') }}" class="ui form">
                    @csrf
                    <div class="field">
                        <label for="txt">Nama</label>
                        <input type="text" id="txt" name="txt" placeholder="Input" required>
                    </div>
                    <button class="ui button" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
@stop
