@extends('vendor.laravolt.layouts.centered')
@section('content')
    {!! form()->get(route('complaints.result')) !!}
    {!! form()->select('company', $company)->label('Company Name')->placeholder('Select Company') !!}

    {!! form()->action([
        form()->submit('Search'),
        form()->link('Back', route('auth::login'))
        ])
    !!}

    {!! form()->close() !!}
@endsection

