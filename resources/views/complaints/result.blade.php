@extends('vendor.laravolt.layouts.plain')
@section('content')
    <div class="ui container p-t-3">

        <a href="{{ route('complaints.index') }}" class="ui labeled icon large button">
            <i class="left arrow icon"></i>
            <strong>Back</strong>
        </a>

        <x-panel title="Company and Count of The Response">
            <table class="ui celled table" aria-describedby="company">
                <thead>
                <tr>
                    <th id="company">Company</th>
                    <th id="closed">Closed</th>
                    <th id="closed_with_eplanation">Closed With Explanation</th>
                    <th id="closed_with_non-monetary_relief">Closed With Non-Monetary Relief</th>
                    <th id="closed_with_monetary_relief">Closed With Monetary Relief</th>
                    <th id="untimely_response">Untimely Response</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    @foreach($result as $item)
                        <td>{{ $item->Company }}</td>
                        <td class="center aligned">{{ $item->Closed }}</td>
                        <td class="center aligned">{{ $item->Closed_with_explanation }}</td>
                        <td class="center aligned">{{ $item->Closed_with_non_monetary_relief }}</td>
                        <td class="center aligned">{{ $item->Closed_with_monetary_relief }}</td>
                        <td class="center aligned">{{ $item->Untimely_response }}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        </x-panel>
    </div>
@endsection

