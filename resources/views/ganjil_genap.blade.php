@extends('template')
@section('content')
    <div class="ui divider hidden"></div>
    <div class="ui grid equal">
        <div class="row">
            <div class="ten wide column centered">
                <form method="post" action="{{ route('ganjil-genap.check') }}" class="ui form">
                    @csrf
                    <div class="field">
                        <label for="a">Angka Pertama</label>
                        <input type="number" id="a" name="a" placeholder="Angka Pertama">
                    </div>
                    <div class="field">
                        <label for="b">Angka Kedua</label>
                        <input type="number" id="b" name="b" placeholder="Angka Pertama">
                    </div>
                    <button class="ui button" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
@stop
